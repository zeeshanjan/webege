package com.semantic.core;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import no.s11.owlapijsonld.JsonLdOntologyFormat;
import no.s11.owlapijsonld.JsonLdStorer;

import org.json.JSONObject;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.ConsoleProgressMonitor;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.semanticweb.owlapi.util.DefaultPrefixManager;

public class LoadOntology {

    static File f;// = new File("museum.owl");
    static OWLOntologyManager manager;// =
                                      // OWLManager.createOWLOntologyManager();
    static OWLDataFactory factory;// = manager.getOWLDataFactory();
    static OWLOntology ontology;
    static IRI ontologyIRI;// = ontology.getOntologyID().getOntologyIRI();
    static PrefixManager pm;

    // ontology = manager.loadOntologyFromOntologyDocument(f);
    public static Object[] loadOnt(String ont) {
        try {
            f = new File(ont + ".owl");
            manager = OWLManager.createOWLOntologyManager();
            factory = manager.getOWLDataFactory();
            ontology = manager.loadOntologyFromOntologyDocument(f);
            ontologyIRI = ontology.getOntologyID().getOntologyIRI();

            System.out.println("Loaded ontology: " + ontology.getOntologyID());

            for (OWLClass cls : ontology.getClassesInSignature()) {
                System.out.println("Class: " + cls);
                System.out
                        .println("subClasses: " + cls.getSubClasses(ontology));
                for (OWLIndividual ind : cls.getIndividualsInSignature()) {
                    System.out.println("Individual: " + ind);
                }
            }
            return ontology.getClassesInSignature().toArray();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public static void createOntology(String ont)
            throws OWLOntologyCreationException, OWLOntologyStorageException {

        manager = OWLManager.createOWLOntologyManager();
        IRI ontologyIRI = IRI.create("http://example.org/" + ont);

        ontology = manager.createOntology(ontologyIRI);
        System.out.println("Created ontology: " + ontology);
        OWLOntologyID ontologyID = ontology.getOntologyID();
        System.out.println("Ontology IRI: " + ontologyID.getOntologyIRI());
        File fileformated = new File(ont + ".owl");
        manager.saveOntology(ontology, IRI.create(fileformated.toURI()));
    }

    public static void createClass(String supClaz, String subClaz)
            throws OWLOntologyCreationException, OWLOntologyStorageException {
        manager = OWLManager.createOWLOntologyManager();
        factory = manager.getOWLDataFactory();
        ontology = manager.loadOntologyFromOntologyDocument(f);
        ontologyIRI = ontology.getOntologyID().getOntologyIRI();
        OWLClass superClazz, subClazz;
        AddAxiom addAxiom;
        if (supClaz.equals("Thing")) {
            subClazz = factory.getOWLClass(IRI.create(ontologyIRI + "#"
                    + subClaz));
            // OWLEntity entity = factory.getOWLEntity(EntityType.CLASS,
            // IRI.create(subClaz));
            OWLAxiom addC = factory.getOWLDeclarationAxiom(subClazz);
            manager.addAxiom(ontology, addC);
        }

        else {
            superClazz = factory.getOWLClass(IRI.create(ontologyIRI + "#"
                    + supClaz));
            subClazz = factory.getOWLClass(IRI.create(ontologyIRI + "#"
                    + subClaz));
            OWLAxiom axiom = factory
                    .getOWLSubClassOfAxiom(subClazz, superClazz);
            addAxiom = new AddAxiom(ontology, axiom);
            manager.applyChange(addAxiom);
        }

        /*
         * for (OWLClass cls : ontology.getClassesInSignature()) {
         * System.out.println("Referenced class: " + cls); }
         * 
         * Set<OWLClassExpression> superClasses = subClazz
         * .getSuperClasses(ontology);
         * System.out.println("Asserted superclasses of " + subClazz + ":"); for
         * (OWLClassExpression desc : superClasses) { System.out.println(desc);
         * }
         */
        manager.saveOntology(ontology);
    }

    public static void createIndividual(String clazz, String name) {
        try {
            manager = OWLManager.createOWLOntologyManager();
            factory = manager.getOWLDataFactory();
            ontology = manager.loadOntologyFromOntologyDocument(f);
            ontologyIRI = ontology.getOntologyID().getOntologyIRI();
            pm = new DefaultPrefixManager(ontologyIRI.toString());

            OWLClass indClazz = factory.getOWLClass(IRI.create(ontologyIRI
                    + "#" + clazz));

            OWLNamedIndividual indName = factory.getOWLNamedIndividual(IRI
                    .create(ontologyIRI + "#" + name));
            OWLClassAssertionAxiom classAssertion = factory
                    .getOWLClassAssertionAxiom(indClazz, indName);
            manager.addAxiom(ontology, classAssertion);

            System.out.println("RDF/XML");
            manager.saveOntology(ontology);
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void deleteClass(String clz)
            throws OWLOntologyStorageException {
        OWLClass d = factory.getOWLClass(IRI.create(ontologyIRI + "#" + clz));
        System.out.println("Class: " + d.getIRI().toString());
        Set<OWLAxiom> axiomsToRemove = new HashSet<OWLAxiom>();
        for (OWLAxiom ax : ontology.getAxioms()) {
            if (ax.getSignature().contains(d)) {
                axiomsToRemove.add(ax);
                System.out
                        .println("to remove from "
                                + ontology.getOntologyID().getOntologyIRI()
                                + ": " + ax);
            }
        }
        manager.removeAxioms(ontology, axiomsToRemove);
        manager.saveOntology(ontology);
    }

    public static void deleteIndvidual(String ind)
            throws OWLOntologyStorageException {
        OWLIndividual individual = factory.getOWLNamedIndividual(IRI.create(ontologyIRI + "#" + ind));
        Set<OWLAxiom> axiomsToRemove = new HashSet<OWLAxiom>();
        for (OWLAxiom ax : ontology.getAxioms()) {
            if (ax.getSignature().contains(individual)) {
                axiomsToRemove.add(ax);
                System.out
                        .println("to remove from "
                                + ontology.getOntologyID().getOntologyIRI()
                                + ": " + ax);
            }
        }
        manager.removeAxioms(ontology, axiomsToRemove);
        manager.saveOntology(ontology);
    }

    
    
    public static Set<OWLNamedIndividual> loadIndividuals() {
        return ontology.getIndividualsInSignature();
    }

    public static Set<OWLObjectProperty> getObjectProperties() {
        return ontology.getObjectPropertiesInSignature();
    }

    public static void testH() {
        JsonLdStorer.register(manager);
        try {
            manager.saveOntology(ontology, new JsonLdOntologyFormat(),
                    System.out);
        } catch (OWLOntologyStorageException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void printHierarchy() {
        OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
        ConsoleProgressMonitor progressMonitor = new ConsoleProgressMonitor();
        OWLReasonerConfiguration config = new SimpleConfiguration(
                progressMonitor);
        OWLReasoner reasoner = reasonerFactory.createReasoner(ontology, config);
        reasoner.precomputeInferences();
        boolean consistent = reasoner.isConsistent();
        System.out.println("Consistent: " + consistent);
        System.out.println("\n");
        Node<OWLClass> bottomNode = reasoner.getUnsatisfiableClasses();

        Set<OWLClass> unsatisfiable = bottomNode.getEntitiesMinusBottom();
        if (!unsatisfiable.isEmpty()) {
            System.out.println("The following classes are unsatisfiable: ");
            for (OWLClass cls : unsatisfiable) {
                System.out.println("    " + cls);
            }
        } else {
            System.out.println("There are no unsatisfiable classes");
        }
        System.out.println("\n");

        // OWLDataFactory fac = manager.getOWLDataFactory();

        Node<OWLClass> topNode = reasoner.getTopClassNode();
        print(topNode, reasoner, 0);

        System.out.println("--= Start =--");
        System.out.println(parentData);
        System.out.println("--= End =--");
    }

    public static JSONObject parentData = new JSONObject();
    // public static JSONObject mainData = new JSONObject();
    public static JSONObject childData = new JSONObject();
    public static int i, j, k = 0;

    private static void print(Node<OWLClass> parent, OWLReasoner reasoner,
            int depth) {
        if (parent.isBottomNode()) {
            return;
        }
        // Print an indent to denote parent-child relationships
        printIndent(depth);
        // Now print the node (containing the child classes)
        printNode(parent);

        for (Node<OWLClass> child : reasoner.getSubClasses(
                parent.getRepresentativeElement(), true)) {
            print(child, reasoner, depth + 1);

        }
        parentData.put(parent.toString(), childData);
        i++;
    }

    private static void printIndent(int depth) {
        for (int i = 0; i < depth; i++) {
            System.out.print("    ");
        }
    }

    private static void printNode(Node<OWLClass> node) {
        // JSONObject parentData = new JSONObject();
        // JSONObject childData = new JSONObject();

        childData = new JSONObject();
        j = 0;
        // parentData.put(Integer.toString(i), node.toString());
        DefaultPrefixManager pm = new DefaultPrefixManager(
                ontologyIRI.toString());
        // Print out a node as a list of class names in curly brackets
        System.out.print("{");
        j = 0;
        for (Iterator<OWLClass> it = node.getEntities().iterator(); it
                .hasNext();) {
            OWLClass cls = it.next();
            // User a prefix manager to provide a slightly nicer shorter name
            childData.put(Integer.toString(j), cls.toString());
            j++;
            System.out.print(pm.getShortForm(cls));
            if (it.hasNext()) {
                System.out.print(" ");
            }
        }
        System.out.println("}");
    }

    public static Set<OWLClassExpression> getDomainsOfProperty(String prop) {
        OWLObjectProperty objProp = factory.getOWLObjectProperty(IRI
                .create(ontologyIRI + "#"
                + prop));
        return objProp.getDomains(ontology);
    }

    public static Set<OWLClassExpression> getRangesOfProperty(String prop) {
        OWLObjectProperty objProp = factory.getOWLObjectProperty(IRI.create(ontologyIRI+"#"
                + prop));
        return objProp.getRanges(ontology);
    }

    public static void addObjectProperty(String prop, String domain,
            String range) throws OWLOntologyStorageException {
        // TODO Auto-generated method stub
        OWLClass clsA = factory.getOWLClass(IRI.create(ontologyIRI+"#" + domain));
        OWLClass clsB = factory.getOWLClass(IRI.create(ontologyIRI+"#" + range));

        OWLObjectProperty objProp = factory.getOWLObjectProperty(IRI.create(ontologyIRI+"#"
                + prop));

        OWLObjectPropertyDomainAxiom domainAxiom = factory
                .getOWLObjectPropertyDomainAxiom(objProp, clsA);
        OWLObjectPropertyRangeAxiom rangeAxiom = factory
                .getOWLObjectPropertyRangeAxiom(objProp, clsB);

//        OWLAxiom axiom = factory.getOWLObjectPropertyAssertionAxiom(prop, clsA, clsB);
        
        manager.addAxiom(ontology, domainAxiom);
        manager.addAxiom(ontology, rangeAxiom);
        manager.saveOntology(ontology);
    }
}
