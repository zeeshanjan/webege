package com.semantic.core;

import static spark.Spark.*;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import com.google.gson.Gson;

import spark.*;

public class SemanticApplication {

    /**
     * @param args
     */

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        staticFileLocation("/public");

        get(new Route("/api/hello") {
            @Override
            public Object handle(Request request, Response response) {
                // Gson gson = new Gson();
                return "Hello World";
            }
        });

        get(new Route("/api/getH") {
            @Override
            public Object handle(Request request, Response response) {
                // LoadOntology.loadOnt();
                LoadOntology.printHierarchy();
                return response;
            }
        });

        get(new Route("/api/loadOntology") {
            @Override
            public Object handle(Request request, Response response) {
                Gson gson = new Gson();
                return gson.toJson(LoadOntology.loadOnt(request.queryParams(
                        "name").toString()));
                // return LoadOntology.loadOnt();

            }
        });
        get(new Route("/api/loadIndividuals") {
            @Override
            public Object handle(Request request, Response response) {
                Gson gson = new Gson();
                return gson.toJson(LoadOntology.loadIndividuals());
            }
        });

        get(new Route("/api/getObjectProperties") {
            @Override
            public Object handle(Request request, Response response) {
                Gson gson = new Gson();
                return gson.toJson(LoadOntology.getObjectProperties());
            }
        });

        get(new Route("/api/getDomainOfObjectProperty") {
            @Override
            public Object handle(Request request, Response response) {
                Gson gson = new Gson();
                return gson.toJson(LoadOntology.getDomainsOfProperty(request
                        .queryParams("prop").toString()));
            }
        });

        get(new Route("/api/getRangeOfObjectProperty") {
            @Override
            public Object handle(Request request, Response response) {
                Gson gson = new Gson();
                return gson.toJson(LoadOntology.getRangesOfProperty(request
                        .queryParams("prop").toString()));
            }
        });

        post(new Route("/api/addObjectProperty") {

            @Override
            public Object handle(Request request, Response response) {
                try {
                    LoadOntology.addObjectProperty(request.queryParams("prop")
                            .toString(), request.queryParams("dom").toString(),
                            request.queryParams("ran").toString());
                } catch (OWLOntologyStorageException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return true;
            }
        });

        post(new Route("/api/createOntology") {

            @Override
            public Object handle(Request request, Response response) {
                try {

                    LoadOntology.createOntology(request.queryParams("name")
                            .toString());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                    return false;
                } catch (OWLOntologyStorageException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    return false;
                }
                return true;

            }
        });

        post(new Route("/api/createIndividual") {
            @Override
            public Object handle(Request request, Response response) {
                LoadOntology.createIndividual(request.queryParams("clazz")
                        .toString(), request.queryParams("name").toString());
                return true;

            }
        });

        post(new Route("/api/deleteIndividual") {
            @Override
            public Object handle(Request request, Response response) {
                try {
                    LoadOntology.deleteIndvidual(request.queryParams("ind")
                            .toString());
                } catch (OWLOntologyStorageException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return true;

            }
        });
        
        post(new Route("/api/createClass") {
            @Override
            public Object handle(Request request, Response response) {
                try {
                    LoadOntology
                            .createClass(request.queryParams("clazz")
                                    .toString(), request.queryParams("name")
                                    .toString());
                } catch (OWLOntologyCreationException e) {
                    e.printStackTrace();
                } catch (OWLOntologyStorageException e) {
                    e.printStackTrace();
                }
                return true;
            }
        });

        post(new Route("/api/deleteClass") {
            @Override
            public Object handle(Request request, Response response) {
                try {
                    LoadOntology.deleteClass(request.queryParams("clazz")
                            .toString());
                } catch (OWLOntologyStorageException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                return true;

            }
        });

    }
}
