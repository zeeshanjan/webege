function CtrlObjProp($scope, SemanticService) {
    var loadObjPromise = SemanticService.getAllObjectProperties();
    loadObjPromise.then(function (result) {
        $scope.propList = result;
    });
    if (!SemanticService.newOntology) {
        SemanticService.newOntology = "museum";
    }
    if (!SemanticService.classList) {
        var classListPromise = SemanticService.getAllClasses(SemanticService.newOntology);
        classListPromise.then(function (result) {
            if (result) {
                $scope.classList = result;
                SemanticService.classList = result;
            }
        });
    }
    else {
        $scope.classList = SemanticService.classList;
    }
    $scope.clickedProp = function (prop) {
        console.log("Clicked Object Property: ", prop);
        $scope.selectedProp = prop;
        /*   var domainListPromise = SemanticService.getDomainsOfObjectProperty(prop.iri.remainder);
         domainListPromise.then(function (result) {
         if (result) {
         $scope.selectedDomain = result.iri.remainder;
         $scope.domList = result;
         console.log("Domains: ", result);
         }
         });

         var rangeListPromise = SemanticService.getRangeOfObjectProperty(prop.iri.remainder);
         rangeListPromise.then(function (result) {
         if (result) {
         $scope.selectedRange = result.iri.remainder;
         $scope.ranList = result;
         console.log("Ranges: ", result);
         }
         });*/
        loadDomainsAndRanges(prop);
    }

    var loadDomainsAndRanges = function (prop) {
        console.log("loadDomainsFunction: ", prop);
        var domainListPromise = SemanticService.getDomainsOfObjectProperty(prop.iri.remainder);
        domainListPromise.then(function (result) {
            if (result) {
                $scope.selectedDomain = result;
                $scope.domList = result;
                console.log("Domains: ", result);
            }
        });

        var rangeListPromise = SemanticService.getRangeOfObjectProperty(prop.iri.remainder);
        rangeListPromise.then(function (result) {
            if (result) {
                $scope.selectedRange = result;
                $scope.ranList = result;
                console.log("Ranges: ", result);
            }
        });
    }

    $scope.AddObjectProperty = function () {

        var prop = $scope.selectedProp.iri.remainder;
        var dom = $scope.domClass.iri.remainder;
        var ran = $scope.ranClass.iri.remainder;

        var addObjPropPromise = SemanticService.addObjProperty(prop, dom, ran);
        addObjPropPromise.then(function (result) {
            if (result) {
                console.log("Object Property Added.");
                loadDomainsAndRanges(prop);
            }
        });
    }

}