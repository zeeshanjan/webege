function LoadIndividual($scope, SemanticService) {

    $scope.setThing = function () {
        $scope.selectedClazz = 'Thing';
    }

    $scope.addIndividual = function () {
        // console.log("Class: ", $scope.clazz);
        if (!$scope.selectedClazz || $scope.selectedClazz == 'Thing') {
            alert("Please select class.");
            return;
        }
        var classAddPromise = SemanticService.createIndividual($scope.selectedClazz, $scope.newInd);
        classAddPromise.then(function (result) {
            $scope.newInd = '';
        });

    }

    $scope.loadIndividuals = function () {
        var loadIndPromise = SemanticService.getAllIndividuals();
        loadIndPromise.then(function (result) {
            $scope.indList = result;
        });
    }

    $scope.clickedInd = function (ind) {
        $scope.selectedInd = ind.iri.remainder;
        console.log("Selected Ind: ", $scope.selectedInd);
    }
    $scope.delIndividual = function () {
        if (!$scope.selectedInd) {
            return;
        }
        console.log("Delete Ind: ", $scope.selectedInd);
        var delIndPromise = SemanticService.deleteIndividual($scope.selectedInd);
        delIndPromise.then(function (result) {
            $scope.loadIndividuals();
            $scope.selectedInd = '';
        });
    }
    $scope.clickedClazz = function (clz) {
        console.log(clz);
        if (!clz.iri.remainder) {
            $scope.selectedClazz = 'Thing';
        }
        else {
            $scope.selectedClazz = clz.iri.remainder;
        }
    }
    var init = function () {
        if (!SemanticService.classList) {
            var classListPromise = SemanticService.getAllClasses();
            classListPromise.then(function (result) {
                $scope.classList = result;
                SemanticService.classList = result;
            });
        }
        else {
            $scope.classList = SemanticService.classList;
        }
        $scope.loadIndividuals();
    }
    init();
}
