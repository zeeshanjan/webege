function CtrlMain($scope, $location, SemanticService) {

    var init = function () {
      //  $scope.loadOntology();
    }
    init();
    $scope.setThing = function () {
        $scope.selectedClazz = 'Thing';
    }
    $scope.createOnt = function () {
        if (confirm('are you sure to create new Ontology? ')) {
            var name = prompt("Please enter Ontology name", "MyOntology");
            if (name != null) {
                //alert(name);
                var newOntPromise = SemanticService.createOntology(name);
                newOntPromise.then(function (result) {
                    console.log("Result: ", result);
                    if (result) {
                        SemanticService.newOntology = name;
                        $location.url('/load');
                        $scope.loadOntology();
                        //$route.reload();
                    }
                });
            }
        }
    }

    $scope.loadOntology = function (ont) {
        //$location.url('/load');
        console.log("NewOntology: ", SemanticService.newOntology);
        console.log("ont: ", ont);
        if (!SemanticService.newOntology || ont) {
            //console.log("IF Statement: ",SemanticService.newOntology);
            SemanticService.newOntology = "museum";
        }
        console.log("NewOntology AfterIF: ", SemanticService.newOntology);
        var classListPromise = SemanticService.getAllClasses(SemanticService.newOntology);
        classListPromise.then(function (result) {
            if (result) {
                $scope.classList = result;
                SemanticService.classList = result;
                console.log("Classes: ", $scope.classList);
                $scope.selectedClazz = 'Thing';
                $location.url('/load');
//                $route.reload();
            }
        });
    }

    $scope.clickedClazz = function (clz) {
        console.log(clz);
        if (!clz.iri.remainder) {
            $scope.selectedClazz = 'Thing';
        }
        else {
            $scope.selectedClazz = clz.iri.remainder;
        }
    }

    $scope.addClazz = function () {
        if (!$scope.newC) {
            alert("Please enter class name.");
            return;
        }
        console.log("selectedClazz: ", $scope.selectedClazz);
        console.log("newClass: ", $scope.newC);
        var supClazz;
        if (!$scope.selectedClazz) {
            supClazz = "Thing";
        }
        else {
            supClazz = $scope.selectedClazz;
        }

        var classAddPromise = SemanticService.createClass(supClazz, $scope.newC);
        classAddPromise.then(function (result) {
            if (result) {
                $scope.newC = '';
                $scope.loadOntology();
                $location.url('/load');
//                $route.reload();
            }
        });
    }

    $scope.deleteClazz = function (clz) {
        console.log(clz);
        if (confirm('are you sure to Delete ' + clz + ' ?')) {
            var classDelPromise = SemanticService.deleteClass(clz);
            classDelPromise.then(function (result) {
                if (result) {
                    $scope.loadOntology();
                }
            });
        }

    }
}