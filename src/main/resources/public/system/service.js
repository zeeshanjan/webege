var semanticApp = angular.module('semanticApp', ['ngRoute']);

// configure our routes
semanticApp.config(function ($routeProvider) {

    $routeProvider

        // route for the home page
        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'CtrlMain'
        })
        .when('/load', {
            templateUrl: 'pages/LoadOntology.html',
            controller: 'CtrlMain'
        })
        .when('/ind', {
            templateUrl: 'pages/LoadIndividuals.html',
            controller: 'LoadIndividual'
        })
        .when('/objprop', {
            templateUrl: 'pages/LoadObjectProperties.html',
            controller: 'CtrlObjProp'
        })
        .otherwise({
            redirectTo: 'pages/home.html'});
    ;
    //$locationProvider.html5Mode(true);
});

semanticApp.factory('SemanticService', function ($http, $q) {
    var SemanticService = {};

    var classList;
    var newOntology;

    SemanticService.getOntology = function () {

    }

    SemanticService.createOntology = function (name) {

        var deferred = $q.defer();
        var url = 'api/createOntology?name=' + name;
        var status = [];
        var result = {};
        $http.post(url).
            success(function (dt, st) {
                result = dt;
                status = st;
                deferred.resolve(result);
            }).
            error(function (dt, st) {
                result = dt || "Request failed"; // dt = data; st = status;
                status = st;
            });
        return deferred.promise;
    }

    SemanticService.getAllClasses = function (name) {
        var deferred = $q.defer();
        var url = 'api/loadOntology?name=' + name;
        var status = [];
        var result = {};
        $http.get(url).
            success(function (dt, st) {
                result = dt;
                status = st;
                deferred.resolve(result);
            }).
            error(function (dt, st) {
                result = dt || "Request failed"; // dt = data; st = status;
                status = st;
            });
        return deferred.promise;
    }

    SemanticService.getAllIndividuals = function () {
        var deferred = $q.defer();
        var url = 'api/loadIndividuals';
        var status = [];
        var result = {};
        $http.get(url).
            success(function (dt, st) {
                result = dt;
                status = st;
                deferred.resolve(result);
            }).
            error(function (dt, st) {
                result = dt || "Request failed"; // dt = data; st = status;
                status = st;
            });
        return deferred.promise;
    }
    SemanticService.deleteIndividual = function (ind) {
        var deferred = $q.defer();
        var url = 'api/deleteIndividual?ind=' + ind;
        var status = [];
        var result = {};
        $http.post(url).
            success(function (dt, st) {
                result = dt;
                status = st;
                deferred.resolve(result);
            }).
            error(function (dt, st) {
                result = dt || "Request failed"; // dt = data; st = status;
                status = st;
            });
        return deferred.promise;
    }
    SemanticService.getAllObjectProperties = function () {
        var deferred = $q.defer();
        var url = '/api/getObjectProperties';
        var status = [];
        var result = {};
        $http.get(url).
            success(function (dt, st) {
                result = dt;
                status = st;
                deferred.resolve(result);
            }).
            error(function (dt, st) {
                result = dt || "Request failed"; // dt = data; st = status;
                status = st;
            });
        return deferred.promise;
    }

    SemanticService.getDomainsOfObjectProperty = function (name) {
        var deferred = $q.defer();
        var url = 'api/getDomainOfObjectProperty?prop=' + name;
        var status = [];
        var result = {};
        $http.get(url).
            success(function (dt, st) {
                result = dt;
                status = st;
                deferred.resolve(result);
            }).
            error(function (dt, st) {
                result = dt || "Request failed"; // dt = data; st = status;
                status = st;
            });
        return deferred.promise;
    }
    SemanticService.getRangeOfObjectProperty = function (name) {
        var deferred = $q.defer();
        var url = 'api/getRangeOfObjectProperty?prop=' + name;
        var status = [];
        var result = {};
        $http.get(url).
            success(function (dt, st) {
                result = dt;
                status = st;
                deferred.resolve(result);
            }).
            error(function (dt, st) {
                result = dt || "Request failed"; // dt = data; st = status;
                status = st;
            });
        return deferred.promise;
    }

    SemanticService.addObjProperty = function (prop, dom, ran) {

        var deferred = $q.defer();
        var url = 'api/addObjectProperty?prop=' + prop + '&dom=' + dom + '&ran=' + ran;
        var status = [];
        var result = {};
        $http.post(url).
            success(function (dt, st) {
                result = dt;
                status = st;
                deferred.resolve(result);
            }).
            error(function (dt, st) {
                result = dt || "Request failed"; // dt = data; st = status;
                status = st;
            });
        return deferred.promise;
    }

    SemanticService.createClass = function (clazz, name) {

        var deferred = $q.defer();
        var url = 'api/createClass?clazz=' + clazz + '&name=' + name;
        var status = [];
        var result = {};
        $http.post(url).
            success(function (dt, st) {
                result = dt;
                status = st;
                deferred.resolve(result);
            }).
            error(function (dt, st) {
                result = dt || "Request failed"; // dt = data; st = status;
                status = st;
            });
        return deferred.promise;
    }
    SemanticService.deleteClass = function (clazz) {

        var deferred = $q.defer();
        var url = 'api/deleteClass?clazz=' + clazz;
        var status = [];
        var result = {};
        $http.post(url).
            success(function (dt, st) {
                result = dt;
                status = st;
                deferred.resolve(result);
            }).
            error(function (dt, st) {
                result = dt || "Request failed"; // dt = data; st = status;
                status = st;
            });
        return deferred.promise;
    }
    SemanticService.createIndividual = function (clazz, name) {

        var deferred = $q.defer();
        var url = 'api/createIndividual?clazz=' + clazz + '&name=' + name;
        var status = [];
        var result = {};
        $http.post(url).
            success(function (dt, st) {
                result = dt;
                status = st;
                deferred.resolve(result);
            }).
            error(function (dt, st) {
                result = dt || "Request failed"; // dt = data; st = status;
                status = st;
            });
        return deferred.promise;
    }


    return SemanticService;
});
semanticApp.filter("statusFormatter", function () {
    return function (status) {
        if (status == 100)
            return "Completed";
        else
            return "Pending";
    }
});