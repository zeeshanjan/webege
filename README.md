# README #

Webege is Java based Semantic Web Services and WebClient for generating Ontologies, using Spark (sparkjava.com) and OWL-API.
The web-client would be developed in AngularJS (Using Twitter bootstrap).
Pull requests are appreciated.
### What is this repository for? ###

* Semantic Project
* 0.0.1

### How do I get set up? ###

* Install 'Maven' if you don't have it installed.
* Clone this repo, open it in Eclipse and run it as Application.
* You'll notice it running at port 4567.
* Open 'localhost:4567' in browser. (It'll show you nothing yet, though you can try localhost:4567/api/hello for hello world.